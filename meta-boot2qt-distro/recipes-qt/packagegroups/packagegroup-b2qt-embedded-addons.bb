############################################################################
##
## Copyright (C) 2021 The Qt Company Ltd.
## Contact: https://www.qt.io/licensing/
##
## This file is part of the Boot to Qt meta layer.
##
## $QT_BEGIN_LICENSE:GPL$
## Commercial License Usage
## Licensees holding valid commercial Qt licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and The Qt Company. For licensing terms
## and conditions see https://www.qt.io/terms-conditions. For further
## information use the contact form at https://www.qt.io/contact-us.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3 or (at your option) any later version
## approved by the KDE Free Qt Foundation. The licenses are as published by
## the Free Software Foundation and appearing in the file LICENSE.GPL3
## included in the packaging of this file. Please review the following
## information to ensure the GNU General Public License requirements will
## be met: https://www.gnu.org/licenses/gpl-3.0.html.
##
## $QT_END_LICENSE$
##
############################################################################

DESCRIPTION = "Device Creation specific Qt packages"
LICENSE = "The-Qt-Company-Commercial"

inherit packagegroup

PACKAGEGROUP_DISABLE_COMPLEMENTARY = "1"

USE_QT_DEMO_LAUNCHER ?= "${@bb.utils.contains('DISTRO_FEATURES', 'wayland', \
    bb.utils.vercmp_string_op(d.getVar('QT_VERSION'), '6.7', '>'), False, d)}"

USE_PYSIDE ?= "${@bb.utils.vercmp_string_op(d.getVar('QT_VERSION'), '6.7.0', '>') & \
    bb.utils.vercmp_string_op(d.getVar('QT_VERSION'), '6.8', '<')}"

RDEPENDS:${PN} += " \
    ${@'boot2qt-demolauncher' if bb.utils.to_boolean(d.getVar('USE_QT_DEMO_LAUNCHER')) else ''} \
    boot2qt-appcontroller \
    boot2qt-startupscreen \
    qdb \
    qtdeclarative-tools \
    qttools-tools \
    ${@'squish' if bb.utils.to_boolean(d.getVar('USE_SQUISH')) else ''} \
    ${@'python3-pyside6' if bb.utils.to_boolean(d.getVar('USE_PYSIDE')) else ''} \
    "
